package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import paymentapp.brst.com.cast.R;

/**
 * Created by brst-pc89 on 9/7/17.
 */

public class FindCastSchdAdaptor extends RecyclerView.Adapter<FindCastSchdAdaptor.ViewHolder> {

    Context context;
    List<String> listAvailableGig;

    public FindCastSchdAdaptor(Context context, List<String> listAvailableGig) {
        this.context=context;
        this.listAvailableGig=listAvailableGig;
    }


    @Override
    public FindCastSchdAdaptor.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.custom_findavailablegigs,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FindCastSchdAdaptor.ViewHolder holder, int position) {

        holder.textViewGigName.setText(listAvailableGig.get(position));
        holder.textViewCastNum.setText(String.valueOf(32));
        holder.textViewDay.setVisibility(View.GONE);
        if(position%2==0)
        {
            holder.linearLayoutAvailable.setBackgroundColor(context.getResources().getColor(R.color.colorViolet));
        }
    }

    @Override
    public int getItemCount() {
        return listAvailableGig.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewGigName,textViewCastNum,textViewDay;
        LinearLayout linearLayoutAvailable;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewGigName=(TextView)itemView.findViewById(R.id.textViewGigName);
            textViewCastNum=(TextView)itemView.findViewById(R.id.textViewCastNum);
            textViewDay=(TextView)itemView.findViewById(R.id.textViewDay);
            linearLayoutAvailable=(LinearLayout) itemView.findViewById(R.id.linearLayoutAvailable);
        }
    }
}
