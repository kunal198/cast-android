package paymentapp.brst.com.cast.classes;

/**
 * Created by brst-pc89 on 9/5/17.
 */

public class ActiveGigModel {

    String gigName;
    String gigDesc;
    String gigDate;
    Boolean isSelected;

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    int image;
    int imagedot;
    int type;

    public int getImagedot() {
        return imagedot;
    }

    public void setImagedot(int imagedot) {
        this.imagedot = imagedot;
    }



    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }



    public String getGigDate() {
        return gigDate;
    }

    public void setGigDate(String gigDate) {
        this.gigDate = gigDate;
    }



    public String getGigDesc() {
        return gigDesc;
    }

    public void setGigDesc(String gigDesc) {
        this.gigDesc = gigDesc;
    }



    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getGigName() {
        return gigName;
    }

    public void setGigName(String gigName) {
        this.gigName = gigName;
    }
}
