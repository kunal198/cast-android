package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.AvailableGigAdaptor;
import paymentapp.brst.com.cast.classes.ReplaceFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class AvailableGigsFragment extends Fragment implements View.OnClickListener{

    View view;

    RecyclerView availableRV;
    LinearLayout layoutLL;

    String gigs[]={"Gig Title ic_shortlistcheck","Gig Title ic_shortlistcheck","Gig Title 3","Gig Title 4"};

    public AvailableGigsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_available_gigs, container, false);

            bindViews();

            setAdaptor();


        }
        return view;
    }

    public void bindViews() {

        availableRV=(RecyclerView)view.findViewById(R.id.availableRV);
        layoutLL=(LinearLayout) view.findViewById(R.id.layoutLL);

        layoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ReplaceFragment.getInstance().replace((MainActivity) getActivity(),new JobFrgment());
            }
        });
    }


    public void setAdaptor() {

        AvailableGigAdaptor availadleGigAdaptor=new AvailableGigAdaptor(getContext(),gigs);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        availableRV.setLayoutManager(mLayoutManager);
        availableRV.setAdapter(availadleGigAdaptor);


    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_back);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setImageResource(R.mipmap.ic_search);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(10,10,10,10);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("Available Gig");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.GONE);

        ((MainActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imageViewDrawer:

                ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());

                break;
        }
    }
}
