package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.FindGigsAvailableAdaptor;
import paymentapp.brst.com.cast.adaptors.FindScheduledAdaptor;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.sharedpreference.SharedPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class FindGigsFragment extends Fragment implements View.OnClickListener {

    View view;
    RecyclerView recyclerViewFindAvail, recyclerViewFindSched;

    List<String> listAvailableGig;
    List<String> listScheduledGig;

    Fragment fragment;

    Boolean isChecked;

    public FindGigsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_find_gigs, container, false);

            isChecked = SharedPreference.getInstance().getData(getContext());

            bindViews();

            setAvailableAdaptor();

            setScheduledAdaptor();
        }
        return view;
    }

    private void bindViews() {

        recyclerViewFindAvail = (RecyclerView) view.findViewById(R.id.recyclerViewFindAvail);
        recyclerViewFindSched = (RecyclerView) view.findViewById(R.id.recyclerViewFindSched);

    }


    private void setAvailableAdaptor() {
        listAvailableGig = new ArrayList<>();
        listAvailableGig.add("Rebeca's Gig");
        listAvailableGig.add("Dawn's Gig Gig");
        listAvailableGig.add("Mike's Gig");
        listAvailableGig.add("Rebeca's Gig");
        listAvailableGig.add("Dawn's Gig Gig");
        listAvailableGig.add("Mike's Gig");

        FindGigsAvailableAdaptor findAvailableGigAdaptor = new FindGigsAvailableAdaptor(getContext(), listAvailableGig);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewFindAvail.setLayoutManager(mLayoutManager);
        recyclerViewFindAvail.setAdapter(findAvailableGigAdaptor);

    }

    private void setScheduledAdaptor() {
        listScheduledGig = new ArrayList<>();
        listScheduledGig.add("1");
        listScheduledGig.add("2");
        listScheduledGig.add("3");
        listScheduledGig.add("4");
        listScheduledGig.add("5");

        FindScheduledAdaptor findScheduledAdaptor = new FindScheduledAdaptor(getContext(), listScheduledGig);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewFindSched.setLayoutManager(mLayoutManager);
        recyclerViewFindSched.setAdapter(findScheduledAdaptor);
    }

    @Override
    public void onResume() {
        super.onResume();


        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_drawer);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setImageResource(R.mipmap.ic_search);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(0, 0, 0, 0);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("Find Gigs");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.GONE);


        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);

        if (isChecked) {
            ((MainActivity) getActivity()).imageViewDrawerSearch.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.imageViewDrawer:

                Boolean  isChecked= SharedPreference.getInstance().getData(getContext());
                if(!isChecked)
                {

                    if(!MainActivity.listNavigationText.contains("Invite Castees")) {
                        MainActivity.listNavigationText.add(1, "Invite Castees");
                    }
                }
                else
                {
                    if(MainActivity.listNavigationText.contains("Invite Castees")) {
                        MainActivity.listNavigationText.remove("Invite Castees");
                    }
                }

                MainActivity.navigationAdaptor.addData(MainActivity.listNavigationText);


                ((MainActivity) getActivity()).drawer.openDrawer(((MainActivity) getActivity()).navigationView);

                break;


            case R.id.imageViewDrawerSearch:

                try {
                    ReplaceFragment.getInstance().replace((MainActivity) getActivity(), new GigCalender());
                }
                catch (Exception e)
                {

                }


                break;
        }
    }
}
