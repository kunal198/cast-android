package paymentapp.brst.com.cast.frontScreens;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.mainScreens.MainActivity;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener
{

    String TAG=LoginActivity.class.getSimpleName();

    View view;

    Context context;

    EditText editTextName;
    EditText editTextPassword;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        view = findViewById(R.id.main);
        context = this;
        setUpIds();
    }

    private void setUpIds()
    {

        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);


        findViewById(R.id.textViewSignIn).setOnClickListener(this);
        findViewById(R.id.textViewSignUp).setOnClickListener(this);
    }



    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.textViewSignIn:

                startActivity(new Intent(context, MainActivity.class));
               finish();
                //TestingSleep2();
               break;

            case R.id.textViewSignUp:

                Intent intent = new Intent(context, SignUpActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);


                break;

        }
    }

}
