package paymentapp.brst.com.cast.frontScreens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.mainScreens.MainActivity;


public class SignUpActivity extends AppCompatActivity  implements View.OnClickListener
{

    String TAG=LoginActivity.class.getSimpleName();

    View view;

    Context context;

    Toolbar toolbar;

    EditText editTextName;
    EditText editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        view = findViewById(R.id.main);
        context = this;
        setUpIds();
    }

    private void setUpIds()
    {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back);


        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);


        findViewById(R.id.textViewContinue).setOnClickListener(this);
        findViewById(R.id.textViewSignUp).setOnClickListener(this);
        findViewById(R.id.textViewCreate).setOnClickListener(this);
    }



    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
              case R.id.textViewContinue:

//                startActivity(new Intent(context, MainActivityNew.class));
//                finish();
//                TestingSleep2();
                 break;

            case R.id.textViewSignUp:

                finish();

                break;

            case R.id.textViewCreate:

                Intent intent=new Intent(SignUpActivity.this,MainActivity.class);
                intent.putExtra("employee","employee");
                startActivity(intent);
                break;

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:

                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
