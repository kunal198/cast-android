package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import paymentapp.brst.com.cast.R;

/**
 * Created by brst-pc89 on 9/11/17.
 */

public class CalenderAdaptor extends RecyclerView.Adapter<CalenderAdaptor.ViewHolder> {

    List<String> listCalender;
    Context context;


    public CalenderAdaptor(Context context, List<String> listCalender) {

        this.context=context;
        this.listCalender=listCalender;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.custom_calenderview,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.textViewCal.setText(listCalender.get(position));
    }

    @Override
    public int getItemCount() {
        return listCalender.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewCal;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewCal=(TextView)itemView.findViewById(R.id.textViewCal);

        }
    }
}
