package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebBackForwardList;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.QuestionAdaptor;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.mainScreens.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoQuestionFragment extends Fragment implements View.OnClickListener {

    View view;
    ImageView imageViewQuesSubmit;
    TextView textViewAdd;
    RecyclerView recyclerViewQuestion;

    List<String> listQuestion=new ArrayList<>();

    QuestionAdaptor questionAdaptor;

    public VideoQuestionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_videoquestion, container, false);

            bindViews();
            setAdaptor();


        }

        return view;
    }

    private void setAdaptor() {

        listQuestion.add("Question ic_shortlistcheck");

        questionAdaptor=new QuestionAdaptor(getContext(),listQuestion);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getContext());
        recyclerViewQuestion.setLayoutManager(layoutManager);
        recyclerViewQuestion.setAdapter(questionAdaptor);
    }


    private void bindViews() {

        recyclerViewQuestion=(RecyclerView)view.findViewById(R.id.recyclerViewQuestion);

        view.findViewById(R.id.textViewAdd).setOnClickListener(this);
        view.findViewById(R.id.imageViewQuesSubmit).setOnClickListener(this);
        view.findViewById(R.id.imageViewVideoBack).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.imageViewQuesSubmit:

                ReplaceFragment.getInstance().replace((MainActivity) getActivity(), new PostGigFragment());

                break;

            case R.id.textViewAdd:

               listQuestion.add("Question");
                questionAdaptor.addData(listQuestion);
                break;

            case R.id.imageViewVideoBack:

                ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.GONE);

        ((MainActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }
}
