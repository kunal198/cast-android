package paymentapp.brst.com.cast.mainScreens;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.NavigationAdaptor;
import paymentapp.brst.com.cast.classes.Dialog;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.fragment.ActiveGigFragment;
import paymentapp.brst.com.cast.fragment.ApplicationFormFragment;
import paymentapp.brst.com.cast.fragment.AvailableGigsFragment;
import paymentapp.brst.com.cast.fragment.CalenderFragment;
import paymentapp.brst.com.cast.fragment.ChatFragment;
import paymentapp.brst.com.cast.fragment.CreateGigFragment;
import paymentapp.brst.com.cast.fragment.EmployerFragment;
import paymentapp.brst.com.cast.fragment.FindCasteesFragment;
import paymentapp.brst.com.cast.fragment.FindGigsFragment;
import paymentapp.brst.com.cast.fragment.GigCalender;
import paymentapp.brst.com.cast.fragment.InviteCasteesFragment;
import paymentapp.brst.com.cast.fragment.JobCalender;
import paymentapp.brst.com.cast.fragment.NotificationFragment;
import paymentapp.brst.com.cast.fragment.ProfileFragment;
import paymentapp.brst.com.cast.fragment.SettingFragment;
import paymentapp.brst.com.cast.fragment.ShortlistFragment;
import paymentapp.brst.com.cast.fragment.UserProfileFragment;
import paymentapp.brst.com.cast.interfaces.ClickInterface;
import paymentapp.brst.com.cast.sharedpreference.SharedPreference;

public class MainActivity extends AppCompatActivity implements ClickInterface, View.OnClickListener {

    FrameLayout containerFL;
    AvailableGigsFragment availableGigsFragment;
    RecyclerView recyclerViewNavigation;

    public ImageView imageViewDrawer, imageViewDrawerSearch;
    public View viewDrawer;

    public static List<String> listNavigationText;

    public TextView textViewDrawer;
    public LinearLayout linearLayoutBottom, linearLayoutHeader;

    public NavigationView navigationView;
    public DrawerLayout drawer;

    Boolean isChecked;
    public static NavigationAdaptor navigationAdaptor;

    Boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isChecked = SharedPreference.getInstance().getData(getApplicationContext());


        bindViews();

        initFragments();


        setNavigationAdaptor();

        if (getIntent().getExtras() != null) {
            String data = getIntent().getStringExtra("employee");

            ReplaceFragment.getInstance().replace(MainActivity.this,new EmployerFragment());
        } else {
            setFragment();
        }

    }


    private void bindViews() {

        containerFL = (FrameLayout) findViewById(R.id.containerFL);
        recyclerViewNavigation = (RecyclerView) findViewById(R.id.recyclerViewNavigation);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        imageViewDrawer = (ImageView) findViewById(R.id.imageViewDrawer);
        imageViewDrawerSearch = (ImageView) findViewById(R.id.imageViewDrawerSearch);
        viewDrawer = (View) findViewById(R.id.viewDrawer);

        textViewDrawer = (TextView) findViewById(R.id.textViewDrawer);
        linearLayoutBottom = (LinearLayout) findViewById(R.id.linearLayoutBottom);
        linearLayoutHeader = (LinearLayout) findViewById(R.id.linearLayoutHeader);

        findViewById(R.id.linearLayoutHome).setOnClickListener(this);
        findViewById(R.id.linearLayoutNotification).setOnClickListener(this);
        findViewById(R.id.linearLayoutProfile).setOnClickListener(this);
        findViewById(R.id.linearLayoutGigs).setOnClickListener(this);


        imageViewDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!isChecked) {

                    listNavigationText.add(1, "Invite Castees");
                } else {

                    listNavigationText.remove(1);
                }

                navigationAdaptor.addData(listNavigationText);
                drawer.openDrawer(navigationView);
            }
        });
    }

    private void initFragments() {

        availableGigsFragment = new AvailableGigsFragment();

    }

    private void setFragment() {

        if (isChecked) {
            ReplaceFragment.getInstance().replace(this, new FindGigsFragment());
        } else {
            ReplaceFragment.getInstance().replace(this, new FindCasteesFragment());
        }
    }


    private void setNavigationAdaptor() {


        listNavigationText = new ArrayList<>();

        listNavigationText.add("Home");
        if (!isChecked) {
            listNavigationText.add("Invite Castees");
        }
        listNavigationText.add("Setting");
        listNavigationText.add("Logout");
        navigationAdaptor = new NavigationAdaptor(getBaseContext(), listNavigationText);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
        recyclerViewNavigation.setLayoutManager(layoutManager);
        recyclerViewNavigation.setAdapter(navigationAdaptor);

        navigationAdaptor.setClickListener(this);
    }


    @Override
    public void onClick(View view, int position) {

        isChecked = SharedPreference.getInstance().getData(getApplicationContext());
        if (position == 0) {
            //  ReplaceFragment.getInstance().replace(MainActivity.this, new FindCasteesFragment());

            if (isChecked) {
                ReplaceFragment.getInstance().replace(this, new FindGigsFragment());
            } else {
                ReplaceFragment.getInstance().replace(this, new FindCasteesFragment());
            }

            drawer.closeDrawers();

        } else if (!isChecked) {

            if (position == 1) {
                ReplaceFragment.getInstance().replace(MainActivity.this, new InviteCasteesFragment());
                drawer.closeDrawers();

            } else if (position == 2) {
                Bundle bundle = new Bundle();

                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.containerFL);

                if (fragment instanceof FindGigsFragment || fragment instanceof FindCasteesFragment) {

                    bundle.putInt("fragment", 0);
                } else if (fragment instanceof ProfileFragment || fragment instanceof UserProfileFragment) {

                    bundle.putInt("fragment", 1);
                } else if (fragment instanceof ActiveGigFragment) {

                    bundle.putInt("fragment", 2);
                } else {

                    bundle.putInt("fragment", 3);
                }

                ReplaceFragment.getInstance().popBackStack(MainActivity.this);

                SettingFragment settingFragment = new SettingFragment();
                settingFragment.setArguments(bundle);
                ReplaceFragment.getInstance().replace(MainActivity.this, settingFragment);
                drawer.closeDrawers();

            } else {
                Dialog.getInstance().logoutDialog(MainActivity.this);
            }
        } else {
            if (position == 1) {

                Bundle bundle = new Bundle();

                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.containerFL);

                if (fragment instanceof FindGigsFragment || fragment instanceof FindCasteesFragment) {

                    bundle.putInt("fragment", 0);
                } else if (fragment instanceof ProfileFragment || fragment instanceof UserProfileFragment) {

                    bundle.putInt("fragment", 1);
                } else if (fragment instanceof ActiveGigFragment) {

                    bundle.putInt("fragment", 2);
                } else {

                    bundle.putInt("fragment", 3);
                }

                ReplaceFragment.getInstance().popBackStack(MainActivity.this);

                SettingFragment settingFragment = new SettingFragment();
                settingFragment.setArguments(bundle);
                ReplaceFragment.getInstance().replace(MainActivity.this, settingFragment);
                drawer.closeDrawers();
            } else {
                Dialog.getInstance().logoutDialog(MainActivity.this);
            }
        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.linearLayoutHome:


                isChecked = SharedPreference.getInstance().getData(getApplicationContext());
                if (isChecked) {
                    ReplaceFragment.getInstance().replace(this, new FindGigsFragment());
                } else {
                    ReplaceFragment.getInstance().replace(this, new FindCasteesFragment());
                }

                break;

            case R.id.linearLayoutNotification:

                ReplaceFragment.getInstance().replace(MainActivity.this, new NotificationFragment());

                break;

            case R.id.linearLayoutProfile:

                isChecked = SharedPreference.getInstance().getData(getApplicationContext());

                if (isChecked) {
                    ReplaceFragment.getInstance().replace(MainActivity.this, new ProfileFragment());
                } else {
                    ReplaceFragment.getInstance().replace(MainActivity.this, new UserProfileFragment());
                }

                break;

            case R.id.linearLayoutGigs:

                ReplaceFragment.getInstance().replace(MainActivity.this, new ActiveGigFragment());

                break;
        }
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (getIntent().getExtras() != null) {

            if (count!=1) {
                super.onBackPressed();
            }
            else {
                finish();
            }
        } else {
            if (count != 1) {

                super.onBackPressed();

                drawer.closeDrawers();

            } else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    drawer.closeDrawers();

                    finish();
                }
                drawer.closeDrawers();
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        }
    }
}
