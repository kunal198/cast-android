package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.ShortlistAdaptor;
import paymentapp.brst.com.cast.adaptors.UserGridAdaptor;
import paymentapp.brst.com.cast.classes.ActiveGigModel;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.mainScreens.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShortlistFragment extends Fragment implements View.OnClickListener{

    View view;

    GridView gridviewShortlist;

    List<ActiveGigModel> listShortlist;

    ImageView imageViewinvite,imageViewRemove;

    public ShortlistFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_shortlist, container, false);

            bindViews();

            setAdaptor();
        }
        return view;
    }

    private void bindViews() {

        gridviewShortlist = (GridView) view.findViewById(R.id.gridviewShortlist);
        imageViewRemove = (ImageView) view.findViewById(R.id.imageViewRemove);
        imageViewinvite = (ImageView) view.findViewById(R.id.imageViewinvite);
        view.findViewById(R.id.textViewGo).setOnClickListener(this);
        view.findViewById(R.id.linearLayoutSelected).setOnClickListener(this);
        view.findViewById(R.id.linearLayoutRemove).setOnClickListener(this);
    }

    ActiveGigModel activeGigModel;

    private void setAdaptor() {
        listShortlist = new ArrayList<>();

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("INVITED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("BOOKED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("INVITED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("BOOKED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("BOOKED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("BOOKED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("INVITED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("BOOKED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("INVITED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("BOOKED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("APPLIED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("APPLIED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);

        activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("username");
        activeGigModel.setGigDesc("APPLIED");
        activeGigModel.setImage(R.mipmap.ic_shortlistuncheck);
        activeGigModel.setSelected(false);
        listShortlist.add(activeGigModel);


        ShortlistAdaptor shortlistAdaptor = new ShortlistAdaptor(getContext(), listShortlist);
        gridviewShortlist.setAdapter(shortlistAdaptor);
}

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.GONE);
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.textViewGo:

                ReplaceFragment.getInstance().replace((MainActivity) getActivity(),new ChatFragment());
                break;

            case R.id.linearLayoutSelected:

                Log.d("sd","cdsf");
               String tag=imageViewinvite.getTag().toString();
                if (tag.equals("invite"))
                {
                    imageViewinvite.setImageResource(R.mipmap.ic_removeselected);
                    imageViewRemove.setImageResource(R.mipmap.ic_notidotwhite);
                    imageViewinvite.setTag("remove");
                    imageViewRemove.setTag("remove");
                }
                else
                {
                    imageViewinvite.setImageResource(R.mipmap.ic_notidotwhite);
                    imageViewRemove.setImageResource(R.mipmap.ic_removeselected);
                    imageViewinvite.setTag("invite");
                    imageViewRemove.setTag("invite");
                }

                break;


            case R.id.linearLayoutRemove:

                Log.d("sd","cdsfdsf");
                String tag1=imageViewRemove.getTag().toString();
                if (tag1.equals("invite"))
                {
                    imageViewRemove.setImageResource(R.mipmap.ic_notidotwhite);
                    imageViewinvite.setImageResource(R.mipmap.ic_removeselected);
                    imageViewRemove.setTag("remove");
                    imageViewinvite.setTag("remove");
                }
                else
                {
                    imageViewRemove.setImageResource(R.mipmap.ic_removeselected);
                    imageViewinvite.setImageResource(R.mipmap.ic_notidotwhite);
                    imageViewRemove.setTag("invite");
                    imageViewinvite.setTag("invite");
                }
                break;
        }
    }
}
