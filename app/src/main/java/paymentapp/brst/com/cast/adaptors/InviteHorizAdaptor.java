package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import paymentapp.brst.com.cast.R;

/**
 * Created by brst-pc89 on 9/6/17.
 */

public class InviteHorizAdaptor extends RecyclerView.Adapter<InviteHorizAdaptor.ViewHolder> {

    Context context;
    String name[];
    public InviteHorizAdaptor(Context context, String[] name) {

        this.context=context;
        this.name=name;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.custom_invitecasthoriz,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if(position==0)
        {
            holder.imageViewCheck.setVisibility(View.GONE);
        }

        holder.textViewUserName.setText(name[position]);
    }

    @Override
    public int getItemCount() {
        return name.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewUserName;
        ImageView imageViewCheck;
        public ViewHolder(View itemView) {
            super(itemView);

            textViewUserName=(TextView)itemView.findViewById(R.id.textViewUserName);
            imageViewCheck=(ImageView) itemView.findViewById(R.id.imageViewCheck);
        }
    }
}
