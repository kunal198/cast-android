package paymentapp.brst.com.cast.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.ActiveGigAdaptor;
import paymentapp.brst.com.cast.classes.ActiveGigModel;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.sharedpreference.SharedPreference;


public class ActiveGigFragment extends Fragment implements View.OnClickListener {

    View view;

    int TYPE_ITEM=1, TYPE_HEADER=0;

    List<ActiveGigModel> gigList;
    RecyclerView recyclerViewActiveGigs;

    TextView textViewCreate;

    Boolean isChecked;


    public ActiveGigFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(view==null)
        {
            view= inflater.inflate(R.layout.fragment_activegig, container, false);

            isChecked= SharedPreference.getInstance().getData(getContext());

            bindViews();

            setAdaptor();

            setListener();
        }

        return view;
    }




    private void bindViews() {


        recyclerViewActiveGigs=(RecyclerView)view.findViewById(R.id.recyclerViewActiveGigs);
        textViewCreate=(TextView)view.findViewById(R.id.textViewCreate);

     /*   recyclerViewActiveGigs.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                ReplaceFragment.getInstance().replace((MainActivity) getActivity(),new ViewGigFragment());
                return true;
            }
        });
*/
        if (isChecked)
        {
            textViewCreate.setVisibility(View.GONE);
        }
        else
        {
            textViewCreate.setVisibility(View.VISIBLE);
        }

    }

    ActiveGigModel activeGigModel;
    private void setAdaptor() {

        gigList=new ArrayList<>();

         activeGigModel=new ActiveGigModel();

        activeGigModel.setType(TYPE_HEADER);
        activeGigModel.setGigName("Open");
        activeGigModel.setGigDesc("");
        activeGigModel.setGigDate("");

        gigList.add(activeGigModel);

        activeGigModel=new ActiveGigModel();

        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Gig ic_shortlistcheck");
        activeGigModel.setGigDesc("\"Supporting Role for Drama");
        activeGigModel.setGigDate("10-26-17 - 12-5-17");
        gigList.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Gig ic_shortlistcheck");
        activeGigModel.setGigDesc("\"Lead Role - Soap Commercial\"");
        activeGigModel.setGigDate("10-26-17 - 12-5-17");
        gigList.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_HEADER);
        activeGigModel.setGigName("Filled");
        activeGigModel.setGigDesc("");
        activeGigModel.setGigDate("");
        gigList.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Gig 6");
        activeGigModel.setGigDesc("\"Background Role - Comedy Film\"");
        activeGigModel.setGigDate("10-26-17 - 12-5-17");

        gigList.add(activeGigModel);

        ActiveGigAdaptor activeGigAdaptor=new ActiveGigAdaptor(getContext(),gigList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewActiveGigs.setLayoutManager(mLayoutManager);
        recyclerViewActiveGigs.setAdapter(activeGigAdaptor);

    }

    private void setListener() {

        textViewCreate.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.textViewCreate:

                ReplaceFragment.getInstance().replace((MainActivity) getActivity(),new ScheduleGigFragment());

                break;

            case R.id.imageViewDrawer:

                Boolean  isChecked= SharedPreference.getInstance().getData(getContext());
                if(!isChecked)
                {

                    if(!MainActivity.listNavigationText.contains("Invite Castees")) {
                        MainActivity.listNavigationText.add(1, "Invite Castees");
                    }
                }
                else
                {
                    if(MainActivity.listNavigationText.contains("Invite Castees")) {
                        MainActivity.listNavigationText.remove("Invite Castees");
                    }
                }

                MainActivity.navigationAdaptor.addData(MainActivity.listNavigationText);

                ((MainActivity) getActivity()).drawer.openDrawer( ((MainActivity) getActivity()).navigationView);

                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_drawer);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setImageResource(R.mipmap.ic_search);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(0,0,0,0);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("Active Gigs");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.GONE);

        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }



}
