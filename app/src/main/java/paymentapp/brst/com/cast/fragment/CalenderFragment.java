package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import io.blackbox_vision.materialcalendarview.view.CalendarView;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.CalenderAdaptor;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.mainScreens.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalenderFragment extends Fragment implements View.OnClickListener {


    View view;
    CalendarView calendarView;

    List<String> listCalender;

    ImageView imageViewCalender;

    RecyclerView recyclerViewCalender;

    public CalenderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_calender, container, false);

            bindViews();

            setAdaptor();

        }
        return view;
    }

    private void setAdaptor() {

        listCalender.add("SUN");
        listCalender.add("SUN");
        listCalender.add("SUN");
        listCalender.add("SUN");
        listCalender.add("SUN");

        CalenderAdaptor calenderAdaptor = new CalenderAdaptor(getContext(), listCalender);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerViewCalender.setLayoutManager(layoutManager);
        recyclerViewCalender.setAdapter(calenderAdaptor);
    }

    private void bindViews() {

        listCalender = new ArrayList<>();

        recyclerViewCalender = (RecyclerView) view.findViewById(R.id.recyclerViewCalender);

        view.findViewById(R.id.imageViewCalender).setOnClickListener(this);


        calendarView = (CalendarView) view.findViewById(R.id.calendar_view);

        calendarView.shouldAnimateOnEnter(true)
                .setFirstDayOfWeek(Calendar.MONDAY)
                .setOnDateClickListener(new CalendarView.OnDateClickListener() {
                    @Override
                    public void onDateClick(@NonNull Date date) {


                    }
                });
                   /* .setOnMonthChangeListener(this::onMonthChange)
                    .setOnDateLongClickListener(this::onDateLongClick)
                    .setOnMonthTitleClickListener(this::onMonthTitleClick);*/

        if (calendarView.isMultiSelectDayEnabled()) {
            calendarView.setOnMultipleDaySelectedListener(new CalendarView.OnMultipleDaySelectedListener() {
                @Override
                public void onMultipleDaySelected(int i, @NonNull List<Date> list) {

                }
            });
        }

        calendarView.update(Calendar.getInstance(Locale.getDefault()));
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_back);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(10, 10, 10, 10);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("Calender");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.GONE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.VISIBLE);

        ((MainActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imageViewDrawer:

                ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());

                break;

            case R.id.imageViewCalender:

                ReplaceFragment.getInstance().replace((MainActivity) getActivity(), new AvailableGigsFragment());

                break;
        }

    }
}
