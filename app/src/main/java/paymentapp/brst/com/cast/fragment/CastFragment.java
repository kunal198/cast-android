package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.CastAdaptor;
import paymentapp.brst.com.cast.classes.ReplaceFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class CastFragment extends Fragment implements View.OnClickListener{


    View  view;

    RecyclerView recyclerViewCast;

    List<String> userNameList=new ArrayList<>();

    final int sdk = android.os.Build.VERSION.SDK_INT;

    TextView textViewBooked,textViewApplied,textViewAll;

    public CastFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       if(view==null) {
           view= inflater.inflate(R.layout.fragment_cast, container, false);

           bindViews();

           setAdaptor();
       }
       return view;
    }

    private void bindViews()
    {
        recyclerViewCast=(RecyclerView)view.findViewById(R.id.recyclerViewCast);
        textViewBooked=(TextView) view.findViewById(R.id.textViewBooked);
        textViewApplied=(TextView) view.findViewById(R.id.textViewApplied);
        textViewAll=(TextView) view.findViewById(R.id.textViewAll);

        textViewBooked.setOnClickListener(this);
        textViewApplied.setOnClickListener(this);
        textViewAll.setOnClickListener(this);
    }

    private void setAdaptor()
    {
        userNameList.add("Dianne Smith");
        userNameList.add("Dianne Smith");
        userNameList.add("Dianne Smith");
        userNameList.add("Dianne Smith");
        userNameList.add("Dianne Smith");

        CastAdaptor castAdaptor=new CastAdaptor(getContext(),userNameList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewCast.setLayoutManager(mLayoutManager);
        recyclerViewCast.setAdapter(castAdaptor);


    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_back);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(10,10,10,10);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("Cast");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.GONE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.VISIBLE);

        ((MainActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {


            case R.id.imageViewDrawer:

                ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());

                break;

            case R.id.textViewBooked:

                textViewBooked.setBackgroundDrawable(getResources().getDrawable(R.drawable.booked_background));
                textViewApplied.setBackgroundResource(0);
                textViewAll.setBackgroundResource(0);

                textViewBooked.setTextColor(getContext().getResources().getColor(R.color.colorVioletLight));
                textViewApplied.setTextColor(getContext().getResources().getColor(R.color.colorWhite));
                textViewAll.setTextColor(getContext().getResources().getColor(R.color.colorWhite));

                break;

            case R.id.textViewApplied:

                textViewApplied.setBackgroundDrawable(getResources().getDrawable(R.drawable.applied_background));
                textViewBooked.setBackgroundResource(0);
                textViewAll.setBackgroundResource(0);

                textViewApplied.setTextColor(getContext().getResources().getColor(R.color.colorVioletLight));
                textViewBooked.setTextColor(getContext().getResources().getColor(R.color.colorWhite));
                textViewAll.setTextColor(getContext().getResources().getColor(R.color.colorWhite));
                break;

            case R.id.textViewAll:

                textViewAll.setBackgroundDrawable(getResources().getDrawable(R.drawable.all_background));
                textViewBooked.setBackgroundResource(0);
                textViewApplied.setBackgroundResource(0);

                textViewAll.setTextColor(getContext().getResources().getColor(R.color.colorVioletLight));
                textViewBooked.setTextColor(getContext().getResources().getColor(R.color.colorWhite));
                textViewApplied.setTextColor(getContext().getResources().getColor(R.color.colorWhite));
                break;


        }
    }

}
