package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.ChatAdaptor;
import paymentapp.brst.com.cast.classes.ActiveGigModel;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.mainScreens.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment implements View.OnClickListener {


    View view;

    RecyclerView recyclerViewChat;

    TextView textViewChatProfile;

    int SENDER = 0, RECIEVE = 1;

    List<ActiveGigModel> listChat;

    public ChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_chat, container, false);

            bindViews();

            setAdaptor();
        }

        return view;
    }

    private void bindViews() {

        textViewChatProfile = (TextView) view.findViewById(R.id.textViewChatProfile);
        recyclerViewChat = (RecyclerView) view.findViewById(R.id.recyclerViewChat);

        String text = "<i><font color='#5A6DD5'>Booked</font>&nbsp;&nbsp;&nbsp;&nbsp;<font color='#ffffff'>10/14/2016</font></i>";

        textViewChatProfile.setText(Html.fromHtml(text));
    }

    private void setAdaptor() {


        listChat = new ArrayList<>();
        ActiveGigModel activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("moderator:");
        activeGigModel.setGigDesc("This booking requires you to have your make-up done prior to arrival.");
        activeGigModel.setType(RECIEVE);
        listChat.add(activeGigModel);

         activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("John Doe:");
        activeGigModel.setGigDesc("Got it!");
        activeGigModel.setType(SENDER);
        listChat.add(activeGigModel);

         activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("company name:");
        activeGigModel.setGigDesc("It looks like call time will be 11, not 10.");
        activeGigModel.setType(RECIEVE);
        listChat.add(activeGigModel);

         activeGigModel = new ActiveGigModel();
        activeGigModel.setGigName("John Doe:");
        activeGigModel.setGigDesc("No problem!!");
        activeGigModel.setType(SENDER);
        listChat.add(activeGigModel);

        ChatAdaptor chatAdaptor = new ChatAdaptor(getContext(),listChat);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getContext());
        recyclerViewChat.setLayoutManager(layoutManager);
        recyclerViewChat.setAdapter(chatAdaptor);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_back);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(10, 10, 10, 10);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.GONE);
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.GONE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.VISIBLE);

        ((MainActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imageViewDrawer:

                ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());

                break;
        }
    }

}
