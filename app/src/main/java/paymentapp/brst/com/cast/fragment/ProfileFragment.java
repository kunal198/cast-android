package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.ProfilePhotoAdaptor;
import paymentapp.brst.com.cast.adaptors.ProfileRatingAdaptor;
import paymentapp.brst.com.cast.adaptors.ProfileVideoAdaptor;
import paymentapp.brst.com.cast.classes.ActiveGigModel;
import paymentapp.brst.com.cast.mainScreens.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    View view;

    RecyclerView recyclerViewRating,recyclerViewVideo,recyclerViewPhoto;

    ActiveGigModel activeGigModel;
    List<ActiveGigModel> listProfile;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_profile, container, false);

            bindViews();

            setRatingAdaptor();

            setPhotoAdaptor();

            setVideoAdaptor();
        }
        return view;
    }

    private void bindViews() {

        recyclerViewRating=(RecyclerView)view.findViewById(R.id.recyclerViewRating);
        recyclerViewVideo=(RecyclerView)view.findViewById(R.id.recyclerViewVideo);
        recyclerViewPhoto=(RecyclerView)view.findViewById(R.id.recyclerViewPhoto);
    }



    private void setRatingAdaptor()
    {
        listProfile=new ArrayList<>();

        activeGigModel=new ActiveGigModel();
        activeGigModel.setGigName("- Mary Taylor");
        activeGigModel.setType(3);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setGigName("- Adam Johnson");
        activeGigModel.setType(2);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setGigName("- Diane Smith");
        activeGigModel.setType(4);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setGigName("- Mary Taylor");
        activeGigModel.setType(1);
        listProfile.add(activeGigModel);
        activeGigModel=new ActiveGigModel();
        activeGigModel.setGigName("- Mary Taylor");
        activeGigModel.setType(3);
        listProfile.add(activeGigModel);

       ProfileRatingAdaptor profileRatingAdaptor=new ProfileRatingAdaptor(getContext(),listProfile);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerViewRating.setLayoutManager(mLayoutManager);
        recyclerViewRating.setAdapter(profileRatingAdaptor);
    }

    private void setPhotoAdaptor()
    {

        listProfile=new ArrayList<>();
        activeGigModel=new ActiveGigModel();
        activeGigModel.setImage(R.mipmap.img_profilephoto1);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setImage(R.mipmap.img_profilephoto2);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setImage(R.mipmap.img_profilephoto1);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setImage(R.mipmap.img_profilephoto2);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setImage(R.mipmap.img_profilephoto1);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setImage(R.mipmap.img_profilephoto2);
        listProfile.add(activeGigModel);

        ProfilePhotoAdaptor profilePhotoAdaptor=new ProfilePhotoAdaptor(getContext(),listProfile);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.HORIZONTAL);
        recyclerViewPhoto.setLayoutManager(staggeredGridLayoutManager);
        recyclerViewPhoto.setAdapter(profilePhotoAdaptor);
    }

    private void setVideoAdaptor()
    {
        listProfile=new ArrayList<>();
        activeGigModel=new ActiveGigModel();
        activeGigModel.setGigName("Video ic_shortlistcheck");
        activeGigModel.setImage(R.mipmap.img_profilevideoimage);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setGigName("Video ic_shortlistcheck Title");
        activeGigModel.setImage(R.mipmap.img_profilevideoimage);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setGigName("Video ic_shortlistcheck");
        activeGigModel.setImage(R.mipmap.img_profilevideoimage);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setGigName("Video ic_shortlistcheck Title");
        activeGigModel.setImage(R.mipmap.img_profilevideoimage);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setGigName("Video ic_shortlistcheck");
        activeGigModel.setImage(R.mipmap.img_profilevideoimage);
        listProfile.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setGigName("Video ic_shortlistcheck Title");
        activeGigModel.setImage(R.mipmap.img_profilevideoimage);
        listProfile.add(activeGigModel);

        ProfileVideoAdaptor profileVideoAdaptor=new ProfileVideoAdaptor(getContext(),listProfile);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerViewVideo.setLayoutManager(mLayoutManager);
        recyclerViewVideo.setAdapter(profileVideoAdaptor);
    }

    @Override
    public void onResume() {
        super.onResume();


        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.GONE);


    }



}
