package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ActiveGigModel;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.fragment.ViewGigFragment;
import paymentapp.brst.com.cast.mainScreens.MainActivity;

/**
 * Created by brst-pc89 on 9/5/17.
 */

public class ActiveGigAdaptor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


   int TYPE_HEADER = 0;
   int TYPE_ITEM = 1;
    List<ActiveGigModel> gigList;
    Context context;

    public ActiveGigAdaptor(Context context, List<ActiveGigModel> gigList) {

        this.context=context;
        this.gigList=gigList;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView ;

        if (viewType == TYPE_ITEM)
        {
        itemView = LayoutInflater.from(context).inflate(R.layout.custom_itemactivegigs, parent, false);
        return new ActiveGigAdaptor.ItemViewHolder(itemView);

        }
        else
        {
            itemView = LayoutInflater.from(context).inflate(R.layout.custom_headergigs, parent, false);
            return new ActiveGigAdaptor.HeaderViewHolder(itemView);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ActiveGigModel activeGigModel=gigList.get(position);

        if(holder instanceof ItemViewHolder)
        {
            ((ItemViewHolder) holder).textViewGigText.setText(activeGigModel.getGigName());
        }
        else if(holder instanceof HeaderViewHolder)
        {
            ((HeaderViewHolder) holder).viewHeader.setVisibility(View.GONE);

            ((HeaderViewHolder) holder).textViewGigHeader.setText(activeGigModel.getGigName());
        }

    }

    @Override
    public int getItemCount() {
        return gigList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView textViewGigText;
        ImageView imageViewActiveGig;
        public ItemViewHolder(View itemView) {
            super(itemView);

            textViewGigText=(TextView)itemView.findViewById(R.id.textViewGigText);
            imageViewActiveGig=(ImageView) itemView.findViewById(R.id.imageViewActiveGig);

            imageViewActiveGig.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ReplaceFragment.getInstance().replace((MainActivity) context,new ViewGigFragment());
                }
            });

        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView textViewGigHeader;
        View viewHeader;
        public HeaderViewHolder(View itemView) {
            super(itemView);

            textViewGigHeader=(TextView)itemView.findViewById(R.id.textViewGigHeader);
            viewHeader=(View) itemView.findViewById(R.id.viewHeader);
        }
    }

    @Override
    public int getItemViewType(int position) {
      //  return super.getItemViewType(position);

        if (isPositionHeader(position))
        {
            return TYPE_HEADER;
        }
        else
        {
            return TYPE_ITEM;
        }
        //return super.getItemViewType(position);
    }

    private boolean isPositionHeader(int position)
    {
        return gigList.get(position).getType()==0;
    }


}
