package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.sharedpreference.SharedPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewGigFragment extends Fragment implements View.OnClickListener {

    View view;

    TextView textViewView;
    LinearLayout linearLayoutAvailable, linearLayoutViewCast;

    Boolean isChecked;

    public ViewGigFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_view_gig, container, false);

            isChecked = SharedPreference.getInstance().getData(getContext());

            bindViews();

        }

        return view;
    }

    private void bindViews() {

        view.findViewById(R.id.imageViewApply).setOnClickListener(this);

        view.findViewById(R.id.textViewCast).setOnClickListener(this);

        linearLayoutViewCast = (LinearLayout) view.findViewById(R.id.linearLayoutViewCast);
        linearLayoutAvailable = (LinearLayout) view.findViewById(R.id.linearLayoutAvailable);
        textViewView = (TextView) view.findViewById(R.id.textViewView);

        if (isChecked) {
            linearLayoutAvailable.setVisibility(View.VISIBLE);
            linearLayoutViewCast.setVisibility(View.GONE);
            textViewView.setText("Apply Now");
        } else {
            linearLayoutAvailable.setVisibility(View.GONE);
            linearLayoutViewCast.setVisibility(View.VISIBLE);

            textViewView.setText("Return");
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.imageViewDrawer:

                Log.d("cc","cx");
                ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());

                break;
            case R.id.imageViewApply:


                if (isChecked) {

                    ReplaceFragment.getInstance().replace((MainActivity) getActivity(), new QuestionFragment());

                } else {

                    ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());
                }

                break;

            case R.id.textViewCast:

                ReplaceFragment.getInstance().replace((MainActivity) getActivity(), new CastFragment());
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_back);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(10, 10, 10, 10);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("View Gig");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.GONE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.VISIBLE);

        ((MainActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }


}
