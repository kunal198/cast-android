package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;


import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.interfaces.ClickInterface;

/**
 * Created by brst-pc89 on 6/30/17.
 */

public class NavigationAdaptor extends RecyclerView.Adapter<NavigationAdaptor.MyViewHolder> {

    List<String> listNavigationText;

    Context context;

    ClickInterface clickInterface;

    public NavigationAdaptor(Context context, List listNavigationText) {

        this.listNavigationText = listNavigationText;
        this.context = context;

    }


    public void setClickListener(ClickInterface clickListener) {
        this.clickInterface = clickListener;
    }

    @Override
    public NavigationAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_menulist, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NavigationAdaptor.MyViewHolder holder, int position) {

        holder.textViewNavigation.setText(listNavigationText.get(position));

    }


    @Override
    public int getItemCount() {

        return listNavigationText.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewNavigation;


        public MyViewHolder(View itemView) {
            super(itemView);


            textViewNavigation = (TextView) itemView.findViewById(R.id.textViewNavigation);

            textViewNavigation.setOnClickListener(this);


        }


        @Override
        public void onClick(View v) {


            if (clickInterface != null) {

                clickInterface.onClick(v, getAdapterPosition());
            }
        }
    }

    public void addData(List<String> listNavigationText)
    {
        this.listNavigationText=listNavigationText;
        notifyDataSetChanged();
    }
}



