package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ActiveGigModel;

/**
 * Created by brst-pc89 on 9/8/17.
 */

public class ShortlistAdaptor extends BaseAdapter {


    Context context;
    LayoutInflater inflater;

    List<ActiveGigModel> listShortlist;

    public ShortlistAdaptor(Context context, List<ActiveGigModel> listShortlist) {
        this.context = context;
        this.listShortlist = listShortlist;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listShortlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = inflater.inflate(R.layout.custom_grid, null);

        final ActiveGigModel activeGigModel = listShortlist.get(position);

        TextView textViewGridName = (TextView) view.findViewById(R.id.textViewGridName);
        TextView textViewGrid = (TextView) view.findViewById(R.id.textViewGrid);
        final ImageView imageViewRate = (ImageView) view.findViewById(R.id.imageViewRate);
        final Boolean selected = activeGigModel.getSelected();


        textViewGridName.setText(activeGigModel.getGigName());
        if (activeGigModel.getGigDesc().equals("BOOKED")) {
            textViewGrid.setTextColor(context.getResources().getColor(R.color.colorBooked));
        } else if (activeGigModel.getGigDesc().equals("INVITED")) {
            textViewGrid.setTextColor(context.getResources().getColor(R.color.colorGreen));
        } else {
            textViewGrid.setTextColor(context.getResources().getColor(R.color.colorWhite));
        }

        textViewGrid.setText(activeGigModel.getGigDesc());

        if (selected)
        {
            Glide.with(context).load(R.mipmap.ic_shortlistcheck).into(imageViewRate);

        }
        else
        {
            Glide.with(context).load(R.mipmap.ic_shortlistuncheck).into(imageViewRate);

        }


        imageViewRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              if (selected)
              {
                  activeGigModel.setSelected(false);
              }
              else
              {
                  activeGigModel.setSelected(true);
              }
              notifyDataSetChanged();

            }
        });
        return view;
    }
}
