package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ActiveGigModel;

/**
 * Created by brst-pc89 on 9/14/17.
 */

public class ChatAdaptor extends RecyclerView.Adapter<ChatAdaptor.ViewHolder>{

    int SENDER = 0, RECIEVE = 1;
    List<ActiveGigModel> listChat;
    Context context;
    public ChatAdaptor(Context context, List<ActiveGigModel> listChat) {

        this.context=context;
        this.listChat=listChat;
    }

    @Override
    public ChatAdaptor.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.custom_chatview,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChatAdaptor.ViewHolder holder, int position) {


        ActiveGigModel activeGigModel=listChat.get(position);
        if(activeGigModel.getType()==SENDER)
        {
            holder.linearLayoutSend.setVisibility(View.VISIBLE);
            holder.linearLayoutReceive.setVisibility(View.GONE);

            holder.textViewSenderName.setText(activeGigModel.getGigName());
            holder.textViewSender.setText(activeGigModel.getGigDesc());
        }

        else
        {
            holder.linearLayoutSend.setVisibility(View.GONE);
            holder.linearLayoutReceive.setVisibility(View.VISIBLE);

            holder.textViewReceiveName.setText(activeGigModel.getGigName());
            holder.textViewReceive.setText(activeGigModel.getGigDesc());

            if (position==0)
            {
                holder.textViewReceiveName.setTextColor(context.getResources().getColor(R.color.colorModerator));
            }
            else if(position==2)
            {
                holder.textViewReceiveName.setTextColor(context.getResources().getColor(R.color.colorCompany));
            }
        }

    }

    @Override
    public int getItemCount() {
        return listChat.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linearLayoutReceive,linearLayoutSend;
        TextView textViewReceiveName,textViewReceive,textViewSenderName,textViewSender;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewReceiveName=(TextView)itemView.findViewById(R.id.textViewReceiveName);
            textViewReceive=(TextView)itemView.findViewById(R.id.textViewReceive);
            textViewSenderName=(TextView)itemView.findViewById(R.id.textViewSenderName);
            textViewSender=(TextView)itemView.findViewById(R.id.textViewSender);

            linearLayoutReceive=(LinearLayout)itemView.findViewById(R.id.linearLayoutReceive);
            linearLayoutSend=(LinearLayout)itemView.findViewById(R.id.linearLayoutSend);
        }
    }
}
