package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import paymentapp.brst.com.cast.R;

/**
 * Created by brst-pc89 on 9/11/17.
 */

public class QuestionAdaptor extends RecyclerView.Adapter<QuestionAdaptor.ViewHolder> {

    Context context;
    List<String> listQuestion;
    public QuestionAdaptor(Context context, List<String> listQuestion) {

        this.context=context;
        this.listQuestion=listQuestion;
    }


    @Override
    public QuestionAdaptor.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.custom_video_question,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QuestionAdaptor.ViewHolder holder, int position) {

        holder.textViewQuestion.setText(listQuestion.get(position));



    }

    @Override
    public int getItemCount() {
        return listQuestion.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView textViewQuestion,textViewDecSec,textViewSecond,textViewIncSec;
        public ViewHolder(View itemView) {
            super(itemView);

            textViewQuestion=(TextView)itemView.findViewById(R.id.textViewQuestion);
            textViewSecond=(TextView)itemView.findViewById(R.id.textViewSecond);
           itemView.findViewById(R.id.textViewDecSec).setOnClickListener(this);
            itemView.findViewById(R.id.textViewIncSec).setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.textViewDecSec:

                    int textDec=Integer.parseInt(textViewSecond.getText().toString());
                    textDec--;

                    textViewSecond.setText(String.valueOf(textDec));

                    break;

                case R.id.textViewIncSec:

                    int textInc=Integer.parseInt(textViewSecond.getText().toString());
                    textInc++;

                    textViewSecond.setText(String.valueOf(textInc));

                    break;
            }
        }
    }

    public void addData(List<String> listQuestion)
    {
        this.listQuestion=listQuestion;
        notifyDataSetChanged();
    }
}
