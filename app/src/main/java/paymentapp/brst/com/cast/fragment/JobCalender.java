package paymentapp.brst.com.cast.fragment;


import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.CalendarUtils;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.ArrayList;
import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.Decorator;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.mainScreens.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobCalender extends Fragment implements View.OnClickListener {

    View view;

    MaterialCalendarView calendarView;

    public JobCalender() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_job_calender, container, false);

            bindViews();

            setUpCalender();
        }

        return view;
    }

    private void bindViews() {

        calendarView = (MaterialCalendarView) view.findViewById(R.id.calendarView);
        view.findViewById(R.id.textViewJob).setOnClickListener(this);


    }

    public void setUpCalender() {
        calendarView.state().edit()
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();

        calendarView.abc();
        calendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);

        calendarView.addDecorator(new Decorator(getContext(), 1));



        int currentMonth = CalendarDay.today().getMonth();

        List<CalendarDay> list1 = new ArrayList<CalendarDay>();
        list1.add(new CalendarDay(2017, currentMonth, 1));
        list1.add(new CalendarDay(2017, currentMonth, 5));
        list1.add(new CalendarDay(2017, currentMonth, 10));
        list1.add(new CalendarDay(2017, currentMonth - 1, 13));
        list1.add(new CalendarDay(2017, currentMonth + 1, 20));
        list1.add(new CalendarDay(2017, currentMonth + 1, 2));
        list1.add(new CalendarDay(2017, currentMonth - 1, 10));
        list1.add(new CalendarDay(2017, currentMonth, 16));
        list1.add(new CalendarDay(2017, currentMonth, 19));
        list1.add(new CalendarDay(2017, currentMonth, 20));
        list1.add(new CalendarDay(2017, currentMonth, 26));
        list1.add(new CalendarDay(2018, currentMonth + 1, 4));
        list1.add(new CalendarDay(2017, currentMonth + 1, 1));
        list1.add(new CalendarDay(2017, currentMonth - 1, 26));
        list1.add(new CalendarDay(2017, currentMonth - 1, 28));
        list1.add(new CalendarDay(2017, currentMonth - 1, 30));


        calendarView.addDecorator(new Decorator(getContext(), list1, R.color.colorGreen, 3));




    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_back);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(10, 10, 10, 10);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("Calendar");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.GONE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.VISIBLE);

        ((MainActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imageViewDrawer:

                ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());

                break;

            case R.id.textViewJob:

                ReplaceFragment.getInstance().replace((MainActivity) getActivity(),new JobFrgment());
                break;
        }
    }
}
