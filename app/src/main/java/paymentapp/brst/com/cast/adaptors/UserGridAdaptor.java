package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ActiveGigModel;

/**
 * Created by brst-pc89 on 9/8/17.
 */

public class UserGridAdaptor extends BaseAdapter{


    Context context;
    LayoutInflater inflater;

    List<ActiveGigModel> name;
    public UserGridAdaptor(Context context, List<ActiveGigModel> name) {
        this.context=context;
        this.name=name;

        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return name.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view=inflater.inflate(R.layout.custom_grid,null);

        ActiveGigModel  activeGigModel=name.get(position);

        TextView textViewGridName=(TextView)view.findViewById(R.id.textViewGridName);
        TextView textViewGrid=(TextView)view.findViewById(R.id.textViewGrid);
        ImageView imageViewRate=(ImageView)view.findViewById(R.id.imageViewRate);

        textViewGrid.setVisibility(View.GONE);
        textViewGridName.setText(activeGigModel.getGigName());

        Glide.with(context).load(activeGigModel.getImage()).into(imageViewRate);
        return view;
    }
}
