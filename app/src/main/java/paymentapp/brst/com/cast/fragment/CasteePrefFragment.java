package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ReplaceFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class CasteePrefFragment extends Fragment implements View.OnClickListener {

    View view;
    ImageView imageViewSearch;

    public CasteePrefFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_castee_pref, container, false);

            bindViews();

            setListener();
        }

        return view;
    }



    private void bindViews() {

        imageViewSearch=(ImageView)view.findViewById(R.id.imageViewSearch);
    }

    private void setListener() {

        imageViewSearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.imageViewSearch:

                ReplaceFragment.getInstance().replace((MainActivity) getActivity(),new GigsFragment());

                break;
        }
    }
}
