package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.InviteHorizAdaptor;
import paymentapp.brst.com.cast.adaptors.InviteVerticalAdaptor;
import paymentapp.brst.com.cast.classes.ReplaceFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class InviteCasteesFragment extends Fragment implements View.OnClickListener{

    View view;
    RecyclerView recyclerViewInviteHorz,recyclerViewInviteVert;

    LinearLayout linearLayoutInvite;

    String name[]={"Courtnet, 22","Anne, 27","Adam, 23","Courtnet, 22","Anne, 27","Adam, 23"};
    String name1[]={"Courtnet","Anne","Adam","Courtnet","Anne"};

    public InviteCasteesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       if(view==null) {
           view= inflater.inflate(R.layout.fragment_invite_castees, container, false);

           bindViews();

           setHorizontalAdaptor();
           setVerticalAdaptor();


       }

       return view;
    }



    private void bindViews() {

        recyclerViewInviteHorz=(RecyclerView)view.findViewById(R.id.recyclerViewInviteHorz);
        recyclerViewInviteVert=(RecyclerView)view.findViewById(R.id.recyclerViewInviteVert);
        linearLayoutInvite=(LinearLayout) view.findViewById(R.id.linearLayoutInvite);

        linearLayoutInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ReplaceFragment.getInstance().replace((MainActivity) getActivity(),new PostGigFragment());
            }
        });
    }

    private void setHorizontalAdaptor() {

        InviteHorizAdaptor inviteHorizAdaptor=new InviteHorizAdaptor(getContext(),name);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerViewInviteHorz.setLayoutManager(mLayoutManager);
        recyclerViewInviteHorz.setAdapter(inviteHorizAdaptor);
    }

    private void setVerticalAdaptor() {

        InviteVerticalAdaptor inviteVerticalAdaptor=new InviteVerticalAdaptor(getContext(),name1);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewInviteVert.setLayoutManager(mLayoutManager);
        recyclerViewInviteVert.setAdapter(inviteVerticalAdaptor);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_back);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(10,10,10,10);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("Invite Castees");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.GONE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.VISIBLE);

        ((MainActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.imageViewDrawer:

               ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());

                break;
        }
    }
}
