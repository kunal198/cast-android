package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ActiveGigModel;

/**
 * Created by brst-pc89 on 9/8/17.
 */

public class ProfileRatingAdaptor extends RecyclerView.Adapter<ProfileRatingAdaptor.ViewHolder> {

    Context context;
    List<ActiveGigModel> listProfile;
    public ProfileRatingAdaptor(Context context, List<ActiveGigModel> listProfile) {

        this.context=context;
        this.listProfile=listProfile;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.custom_rateprofile,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ActiveGigModel activeGigModel=listProfile.get(position);

        holder.textViewRateUser.setText(activeGigModel.getGigName());
        int number=activeGigModel.getType();
        if(number==1)
        {
            holder.imageViewRate2.setVisibility(View.GONE);
            holder.imageViewRate3.setVisibility(View.GONE);
            holder.imageViewRate4.setVisibility(View.GONE);
        }
        else if(number==2)
        {
            holder.imageViewRate3.setVisibility(View.GONE);
            holder.imageViewRate4.setVisibility(View.GONE);
        }
        else if(number==3)
        {
            holder.imageViewRate4.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return listProfile.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewRateUser;
        ImageView imageViewRate1,imageViewRate2,imageViewRate3,imageViewRate4;
        public ViewHolder(View itemView) {
            super(itemView);

            textViewRateUser=(TextView)itemView.findViewById(R.id.textViewRateUser);
            imageViewRate1=(ImageView)itemView.findViewById(R.id.imageViewRate1);
            imageViewRate2=(ImageView)itemView.findViewById(R.id.imageViewRate2);
            imageViewRate3=(ImageView)itemView.findViewById(R.id.imageViewRate3);
            imageViewRate4=(ImageView)itemView.findViewById(R.id.imageViewRate4);
        }
    }
}
