package paymentapp.brst.com.cast.interfaces;

import android.view.View;

/**
 * Created by brst-pc89 on 6/30/17.
 */

public interface ClickInterface {

    public void onClick(View view, int position);


}
