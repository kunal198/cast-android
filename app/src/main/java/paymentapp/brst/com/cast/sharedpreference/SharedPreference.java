package paymentapp.brst.com.cast.sharedpreference;



import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;



/**
 * Created by brst-pc89 on 7/10/17.
 */

public class SharedPreference {

    public  String PREFERENCE_NAME = "cast_pref";
    public String CHECKED_NAME="checked";
    private static SharedPreference instance = null;

    public static SharedPreference getInstance() {
        if (instance == null) {
            instance = new SharedPreference();
        }

        return instance;
    }

    public SharedPreferences getPreference(Context context) {
        return context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public void storeData(Context context, String key, Boolean value) {

        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putBoolean(key, value);
        editor.apply();


    }


    public void clearData(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        if (preferences != null)
            preferences.edit().remove(key).apply();

    }

    public Boolean  getData(Context context) {


        return getPreference(context).getBoolean(CHECKED_NAME, false);
    }
}


