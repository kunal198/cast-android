package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ActiveGigModel;

/**
 * Created by brst-pc89 on 9/8/17.
 */

public class ProfileVideoAdaptor extends RecyclerView.Adapter<ProfileVideoAdaptor.ViewHolder> {

    Context context;
    List<ActiveGigModel> listProfile;
    public ProfileVideoAdaptor(Context context, List<ActiveGigModel> listProfile) {
        this.context=context;
        this.listProfile=listProfile;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.custom_profilevideo,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ActiveGigModel activeGigModel=listProfile.get(position);
        holder.textViewVideo.setText(activeGigModel.getGigName());
        Glide.with(context).load(activeGigModel.getImage()).into(holder.imageViewVideo);

    }

    @Override
    public int getItemCount() {
        return listProfile.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewVideo;
        ImageView imageViewVideo;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewVideo=(TextView)itemView.findViewById(R.id.textViewVideo);
            imageViewVideo=(ImageView) itemView.findViewById(R.id.imageViewVideo);
        }
    }
}
