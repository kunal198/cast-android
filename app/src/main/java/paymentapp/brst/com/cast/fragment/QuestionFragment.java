package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ReplaceFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionFragment extends Fragment implements View.OnClickListener {

    View view;
    ImageView imageViewQuesSubmit;

    public QuestionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_question1, container, false);

            bindViews();


        }

        return view;
    }


    private void bindViews() {

        view.findViewById(R.id.imageViewQuesSubmit).setOnClickListener(this);
        view.findViewById(R.id.imageViewQuesBack).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.imageViewQuesSubmit:

                ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());

                break;

            case R.id.imageViewQuesBack:

                ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());

                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.GONE);

        ((MainActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }
}
