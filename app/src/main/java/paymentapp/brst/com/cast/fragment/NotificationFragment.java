package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.NotificationAdaptor;
import paymentapp.brst.com.cast.classes.ActiveGigModel;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.sharedpreference.SharedPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment implements View.OnClickListener{


    View view;
    RecyclerView recyclerViewNotification;

    ActiveGigModel activeGigModel;

    int TYPE_ITEM=1, TYPE_HEADER=0;

    List<ActiveGigModel> listNotification;

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(view==null) {
            view = inflater.inflate(R.layout.fragment_notification, container, false);

            bindViews();

            setAdaptor();
        }
        return view;
    }

    private void bindViews() {

        recyclerViewNotification=(RecyclerView)view.findViewById(R.id.recyclerViewNotification);

        listNotification=new ArrayList<>();


    }

    private void setAdaptor()
    {
        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_HEADER);
        activeGigModel.setGigName("WEDNESDAY, MARCH 18");
        activeGigModel.setGigDate("");
        activeGigModel.setGigDesc("");
        listNotification.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("You Have an Upcoming Gig");
        activeGigModel.setGigDate("March 19th, 5:00");
        activeGigModel.setGigDesc("");
        activeGigModel.setImage(R.mipmap.ic_clock);
        activeGigModel.setImagedot(R.mipmap.ic_notidotwhite);
        listNotification.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("You Have an Upcoming Gig");
        activeGigModel.setGigDate("Supporting Role Gig March 19th, 5:00");
        activeGigModel.setGigDesc("");
        activeGigModel.setImage(R.mipmap.img_user);
        activeGigModel.setImagedot(R.mipmap.ic_notidot1);
        listNotification.add(activeGigModel);


        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Video Interview");
        activeGigModel.setGigDate("For John Actor's Gig \" Lead Commercial Role\"");
        activeGigModel.setGigDesc("");
        activeGigModel.setImage(R.mipmap.ic_noti2);
        activeGigModel.setImagedot(R.mipmap.ic_notidot1);
        listNotification.add(activeGigModel);


        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Jane Smith has offered you a gig!");
        activeGigModel.setGigDate("Lead Role for independent film March 19th, 5:00");
        activeGigModel.setGigDesc("");
        activeGigModel.setImage(R.mipmap.img_user);
        activeGigModel.setImagedot(R.mipmap.ic_notidot1);
        listNotification.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_HEADER);
        activeGigModel.setGigName("TUESDAY, MARCH 17");
        activeGigModel.setGigDate("");
        activeGigModel.setGigDesc("");
        listNotification.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("You Have an Upcoming Gig");
        activeGigModel.setGigDate("Supporting Role Gig March 19th, 5:00");
        activeGigModel.setGigDesc("");
        activeGigModel.setImage(R.mipmap.img_user);
        activeGigModel.setImagedot(R.mipmap.ic_notidot2);
        listNotification.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("You Have an Upcoming Gig");
        activeGigModel.setGigDate("March 19th, 5:00");
        activeGigModel.setGigDesc("");
        activeGigModel.setImage(R.mipmap.ic_clock);
        activeGigModel.setImagedot(R.mipmap.ic_notidotwhite);
        listNotification.add(activeGigModel);

        NotificationAdaptor notificationAdaptor=new NotificationAdaptor(getContext(),listNotification);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewNotification.setLayoutManager(mLayoutManager);
        recyclerViewNotification.setAdapter(notificationAdaptor);

    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_drawer);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setImageResource(R.mipmap.ic_search);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(0,0,0,0);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("Notifications");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.GONE);


        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.imageViewDrawer:


                Boolean  isChecked= SharedPreference.getInstance().getData(getContext());
                if(!isChecked)
                {

                    if(!MainActivity.listNavigationText.contains("Invite Castees")) {
                        MainActivity.listNavigationText.add(1, "Invite Castees");
                    }
                }
                else
                {
                    if(MainActivity.listNavigationText.contains("Invite Castees")) {
                        MainActivity.listNavigationText.remove("Invite Castees");
                    }
                }

                MainActivity.navigationAdaptor.addData(MainActivity.listNavigationText);

                ((MainActivity) getActivity()).drawer.openDrawer( ((MainActivity) getActivity()).navigationView);

                break;
        }
    }

}
