package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ActiveGigModel;

/**
 * Created by brst-pc89 on 9/8/17.
 */

public class ProfilePhotoAdaptor extends RecyclerView.Adapter<ProfilePhotoAdaptor.ViewHolder> {

    Context context;
    List<ActiveGigModel> listProfile;

    public ProfilePhotoAdaptor(Context context, List<ActiveGigModel> listProfile) {

        this.context = context;
        this.listProfile = listProfile;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.custom_profile_photo, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Log.d("ds",listProfile.size()+"");
        ActiveGigModel activeGigModel=listProfile.get(position);
        Glide.with(context).load(activeGigModel.getImage()).into(holder.imageViewPhoto);
    }

    @Override
    public int getItemCount() {
        return listProfile.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewPhoto;

        public ViewHolder(View itemView) {
            super(itemView);

            imageViewPhoto=(ImageView)itemView.findViewById(R.id.imageViewPhoto);
        }
    }
}
