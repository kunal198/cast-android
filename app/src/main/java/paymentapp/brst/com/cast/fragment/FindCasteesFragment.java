package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.FindCastSchdAdaptor;
import paymentapp.brst.com.cast.adaptors.FindScheduledAdaptor;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.sharedpreference.SharedPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class FindCasteesFragment extends Fragment implements View.OnClickListener{

    View view;
    RecyclerView recyclerViewFindCastRcmd, recyclerViewFindCastSchd;

    List<String> listRecomdGig;
    List<String> listScheduledGig;

    ViewPager viewPagerCastee;
    CircleIndicator indicator;

    LinearLayout viewPagerCountDots;


    public FindCasteesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_find_castees, container, false);

            bindViews();

            setRecomdAdaptor();
            setSchedulAdaptor();
        }
        return view;
    }

    private void bindViews() {

        recyclerViewFindCastRcmd=(RecyclerView)view.findViewById(R.id.recyclerViewFindCastRcmd);
        recyclerViewFindCastSchd=(RecyclerView)view.findViewById(R.id.recyclerViewFindCastSchd);



    }

    private  void setRecomdAdaptor()
    {
        listRecomdGig=new ArrayList<>();
        listRecomdGig.add("Sharon");
        listRecomdGig.add("Evelyn");
        listRecomdGig.add("Marcus");
        listRecomdGig.add("Sharon");
        listRecomdGig.add("Evelyn");
        listRecomdGig.add("Marcus");

        FindCastSchdAdaptor findCastSchdAdaptor=new FindCastSchdAdaptor(getContext(),listRecomdGig);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewFindCastRcmd.setLayoutManager(mLayoutManager);
        recyclerViewFindCastRcmd.setAdapter(findCastSchdAdaptor);
    }

    private  void setSchedulAdaptor()
    {
        listScheduledGig=new ArrayList<>();
        listScheduledGig.add("1");
        listScheduledGig.add("2");
        listScheduledGig.add("3");
        listScheduledGig.add("4");
        listScheduledGig.add("5");

        FindScheduledAdaptor findScheduledAdaptor=new FindScheduledAdaptor(getContext(),listScheduledGig);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerViewFindCastSchd.setLayoutManager(mLayoutManager);
        recyclerViewFindCastSchd.setAdapter(findScheduledAdaptor);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_drawer);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setImageResource(R.mipmap.ic_search);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(0,0,0,0);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("Find Castee");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.GONE);

        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.imageViewDrawer:

              Boolean  isChecked= SharedPreference.getInstance().getData(getContext());
                if(!isChecked)
                {

                    if(!MainActivity.listNavigationText.contains("Invite Castees")) {
                        MainActivity.listNavigationText.add(1, "Invite Castees");
                    }
                }
                else
                {
                    if(MainActivity.listNavigationText.contains("Invite Castees")) {
                        MainActivity.listNavigationText.remove("Invite Castees");
                    }
                }

                MainActivity.navigationAdaptor.addData(MainActivity.listNavigationText);

                ((MainActivity) getActivity()).drawer.openDrawer( ((MainActivity) getActivity()).navigationView);

                break;

            case R.id.imageViewDrawerSearch:

                try {
                    ReplaceFragment.getInstance().replace((MainActivity) getActivity(), new JobCalender());
                }
                catch (Exception e)
                {

                }


                break;
        }
    }
}
