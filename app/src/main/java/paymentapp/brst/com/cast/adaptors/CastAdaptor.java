package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import paymentapp.brst.com.cast.R;

/**
 * Created by brst-pc89 on 9/6/17.
 */

public class CastAdaptor extends RecyclerView.Adapter<CastAdaptor.ViewHolder> {

    Context context;
    List<String> userNameList;
    public CastAdaptor(Context context, List<String> userNameList) {

        this.context=context;
        this.userNameList=userNameList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.custom_cast,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.textViewCastName.setText(userNameList.get(position));

        if(position%2==0)
        {
            holder.linearLayoutCast.setBackgroundColor(context.getResources().getColor(R.color.colorVioletLight));
        }

    }

    @Override
    public int getItemCount() {
        return userNameList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewCastName;
        LinearLayout linearLayoutCast;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewCastName=(TextView)itemView.findViewById(R.id.textViewCastName);
            linearLayoutCast=(LinearLayout) itemView.findViewById(R.id.linearLayoutCast);
        }
    }
}
