package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.NewGigAdaptor;
import paymentapp.brst.com.cast.classes.ActiveGigModel;
import paymentapp.brst.com.cast.classes.ReplaceFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class GigsFragment extends Fragment implements View.OnClickListener {

    View view;
    RecyclerView recyclerViewGigs;

    int TYPE_ITEM=1, TYPE_HEADER=0;

    List<ActiveGigModel> listAddNewGig=new ArrayList<>();

    TextView textViewAddNew;

    public GigsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_gigs, container, false);

            bindviews();

            setAdaptor();

            setListener();
        }

        return view;
    }

    private void bindviews() {

        recyclerViewGigs=(RecyclerView)view.findViewById(R.id.recyclerViewGigs);
        textViewAddNew=(TextView) view.findViewById(R.id.textViewAddNew);
    }

    ActiveGigModel activeGigModel;
    private void setAdaptor()
    {
       activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_HEADER);
        activeGigModel.setGigName("Active");
        listAddNewGig.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Gig ic_shortlistcheck");
        listAddNewGig.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Gig ic_shortlistcheck");
        listAddNewGig.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Gig 3");
        listAddNewGig.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_HEADER);
        activeGigModel.setGigName("Open");
        listAddNewGig.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Gig 4");
        listAddNewGig.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Gig 5");
        listAddNewGig.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_HEADER);
        activeGigModel.setGigName("Saved");
        listAddNewGig.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Gig 6");
        listAddNewGig.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Gig 7");
        listAddNewGig.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Gig 8");
        listAddNewGig.add(activeGigModel);

        activeGigModel=new ActiveGigModel();
        activeGigModel.setType(TYPE_ITEM);
        activeGigModel.setGigName("Gig 9");
        listAddNewGig.add(activeGigModel);

        NewGigAdaptor newGigAdaptor=new NewGigAdaptor(getContext(),listAddNewGig);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewGigs.setLayoutManager(mLayoutManager);
        recyclerViewGigs.setAdapter(newGigAdaptor);

    }

    private void setListener()
    {
        textViewAddNew.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.textViewAddNew:

                ReplaceFragment.getInstance().replace((MainActivity) getActivity(),new InviteCasteesFragment());

                break;
        }

    }
}
