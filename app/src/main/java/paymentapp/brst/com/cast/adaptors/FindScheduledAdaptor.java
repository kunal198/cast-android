package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import paymentapp.brst.com.cast.R;

/**
 * Created by brst-pc89 on 9/7/17.
 */

public class FindScheduledAdaptor extends RecyclerView.Adapter<FindScheduledAdaptor.ViewHolder> {

    Context context;
    List<String> listScheduledGig;
    public FindScheduledAdaptor(Context context, List<String> listScheduledGig) {

        this.context=context;
        this.listScheduledGig=listScheduledGig;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.custom_findschedulegig,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.textViewSchedul.setText(listScheduledGig.get(position));
    }

    @Override
    public int getItemCount() {
        return listScheduledGig.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewSchedul;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewSchedul=(TextView)itemView.findViewById(R.id.textViewSchedul);
        }
    }
}
