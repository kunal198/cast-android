package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import paymentapp.brst.com.cast.R;

/**
 * Created by brst-pc89 on 9/5/17.
 */

public class AvailableGigAdaptor extends RecyclerView.Adapter<AvailableGigAdaptor.ViewHolder> {

    Context context;
    String gigs[];

    public AvailableGigAdaptor(Context context, String[] gigs) {

        this.context = context;
        this.gigs = gigs;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.custom_availablegigs, parent, false);
        return new  ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.gigTitleTV.setText(gigs[position]);

        if (position==2)
        {
            holder.gigIV.setImageResource(R.mipmap.ic_uncheck);
            holder.gigTitleTV.setTextColor(context.getResources().getColor(R.color.colorViolet));
        }

    }


    @Override
    public int getItemCount() {
        return gigs.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView gigTitleTV;
        ImageView gigIV;

        public ViewHolder(View itemView) {
            super(itemView);

            gigTitleTV = (TextView) itemView.findViewById(R.id.gigTitleTV);
            gigIV = (ImageView) itemView.findViewById(R.id.gigIV);
        }


    }
}
