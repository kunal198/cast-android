package paymentapp.brst.com.cast.classes;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.mainScreens.MainActivity;

/**
 * Created by brst-pc89 on 9/6/17.
 */

public class ReplaceFragment {

   static ReplaceFragment instance=new ReplaceFragment();

   public static ReplaceFragment getInstance()
    {
        return instance;
    }

    public void replace(AppCompatActivity activity, Fragment fragment)
    {
        FragmentManager fragmentManager =activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.containerFL, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public  void popBackStack(MainActivity activity) {

        FragmentManager manager =activity.getSupportFragmentManager();
        manager.popBackStack();
    }

}
