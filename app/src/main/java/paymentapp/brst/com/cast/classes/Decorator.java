package paymentapp.brst.com.cast.classes;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Collection;
import java.util.HashSet;

import paymentapp.brst.com.cast.R;

/**
 * Created by brst-pc80 on 9/15/17.
 */

public class Decorator implements DayViewDecorator {
    private int flag=0;
    private Drawable highlightDrawable;
    private Context context;
    private CalendarDay calendarDay;
    private Drawable drawable;
    private HashSet<CalendarDay> dates;
    private int Color;

    public Decorator( Context context,int flag) {
        this.context = context;
        this.flag=flag;
        highlightDrawable = this.context.getResources().getDrawable(R.drawable.circular_background);
    }

    public Decorator(Context context, CalendarDay dates, int resId,int flag) {

        drawable = ContextCompat.getDrawable(context, resId);
        this.calendarDay=dates;
        this.flag=flag;
    }

    public Decorator(Context context, Collection<CalendarDay> dates, int resId,int flag) {
        Color = ContextCompat.getColor(context, resId);
        this.dates = new HashSet<>(dates);
        this.flag=flag;
    }



    @Override
    public boolean shouldDecorate(CalendarDay day) {
        if(flag==1)
        {
            return day.equals(CalendarDay.today());
        }
        else if(flag==2)
        {

            return day.equals(calendarDay);
        }
        else if(flag==3)
        {
            return dates.contains(day);
        }

        else
        {
            return day.equals(calendarDay);
        }

       // return false;
    }

    @Override
    public void decorate(DayViewFacade view) {

        if(flag==1)
        {
            view.setBackgroundDrawable(highlightDrawable);
            view.addSpan(new ForegroundColorSpan(android.graphics.Color.WHITE));
            view.addSpan(new StyleSpan(Typeface.BOLD));
            view.addSpan(new RelativeSizeSpan(1.5f));
        }
        else if(flag==2)
        {

            view.setSelectionDrawable(drawable);

        }
        else if(flag==3)
        {
            view.addSpan(new ForegroundColorSpan(Color));
        }


    }
}
