package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ReplaceFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobFrgment extends Fragment implements View.OnClickListener {

    View view;
    ImageView imageViewSave;


    public JobFrgment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_job_frgment, container, false);

            bindViews();


        }

        return view;
    }


    private void bindViews() {

       view.findViewById(R.id.imageViewSave);
        view.findViewById(R.id.linearLayoutSubmit).setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.linearLayoutSubmit:

                ReplaceFragment.getInstance().replace((MainActivity) getActivity(), new VideoQuestionFragment());
                break;

            case R.id.imageViewDrawer:

                ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());

                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_back);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(10, 10, 10, 10);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("Application For \n\"Name of Job\"");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.GONE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.VISIBLE);

        ((MainActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }
}


