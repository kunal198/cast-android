package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ActiveGigModel;

/**
 * Created by brst-pc89 on 9/7/17.
 */

public class NotificationAdaptor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<ActiveGigModel> listNotification;
    int TYPE_HEADER = 0;
    int TYPE_ITEM = 1;

    public NotificationAdaptor(Context context, List<ActiveGigModel> listNotification) {

        this.context=context;
        this.listNotification=listNotification;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView ;

        if (viewType == TYPE_ITEM)
        {
            itemView = LayoutInflater.from(context).inflate(R.layout.custom_notificationitem, parent, false);
            return new NotificationAdaptor.ItemViewHolder(itemView);

        }
        else
        {
            itemView = LayoutInflater.from(context).inflate(R.layout.custom_notificationheader, parent, false);
            return new NotificationAdaptor.HeaderViewHolder(itemView);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ActiveGigModel activeGigModel=listNotification.get(position);

        if(holder instanceof NotificationAdaptor.ItemViewHolder)
        {
            ((ItemViewHolder) holder).textViewNotification.setText(activeGigModel.getGigName());
            ((ItemViewHolder) holder).textViewNotiDate.setText(activeGigModel.getGigDate());

            Glide.with(context).load(activeGigModel.getImage()).into(((ItemViewHolder) holder).imageViewNotification);
            Glide.with(context).load(activeGigModel.getImagedot()).into(((ItemViewHolder) holder).imageViewDot);
        }
        if(holder instanceof HeaderViewHolder)
        {
            ((HeaderViewHolder) holder).textViewDayTime.setText(activeGigModel.getGigName());
            ((HeaderViewHolder) holder).linearLayoutNotiHead.setBackgroundColor(context.getResources().getColor(R.color.colorViolet));
        }

    }

    @Override
    public int getItemCount() {
        return listNotification.size();
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView textViewNotification,textViewNotiDate;
        ImageView imageViewNotification,imageViewDot;

        public ItemViewHolder(View itemView) {
            super(itemView);

            textViewNotification=(TextView)itemView.findViewById(R.id.textViewNotification);
            textViewNotiDate=(TextView)itemView.findViewById(R.id.textViewNotiDate);
            imageViewNotification=(ImageView) itemView.findViewById(R.id.imageViewNotification);
            imageViewDot=(ImageView) itemView.findViewById(R.id.imageViewDot);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView textViewDayTime;
        LinearLayout linearLayoutNotiHead;
         ;
        public HeaderViewHolder(View itemView) {
            super(itemView);

            textViewDayTime=(TextView)itemView.findViewById(R.id.textViewDayTime);
            linearLayoutNotiHead=(LinearLayout) itemView.findViewById(R.id.linearLayoutNotiHead);

        }
    }

    @Override
    public int getItemViewType(int position) {

        if (isPositionHeader(position))
        {
            return TYPE_HEADER;
        }
        else
        {
            return TYPE_ITEM;
        }
    }

    private boolean isPositionHeader(int position)
    {
        return listNotification.get(position).getType()==0;
    }
}
