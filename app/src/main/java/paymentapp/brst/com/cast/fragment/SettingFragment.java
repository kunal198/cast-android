package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;

import paymentapp.brst.com.cast.classes.Dialog;
import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.sharedpreference.SharedPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment implements View.OnClickListener{


    View view;
    ImageView imageViewSetting;
    Switch switchSetting;

    Boolean  isChecked;

    int fragmentCount;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(view==null) {
            view= inflater.inflate(R.layout.fragment_setting, container, false);

              isChecked= SharedPreference.getInstance().getData(getContext());

            Bundle bundle=getArguments();
            if(bundle!=null)
            {
                fragmentCount=bundle.getInt("fragment");
            }

            bindViews();

            setListener();
        }
        return view;
    }



    private void bindViews() {

        imageViewSetting=(ImageView)view.findViewById(R.id.imageViewSetting);
        switchSetting=(Switch) view.findViewById(R.id.switchSetting);

       switchSetting.setChecked(isChecked);

        switchSetting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                    SharedPreference.getInstance().storeData(getContext(),SharedPreference.getInstance().CHECKED_NAME,isChecked);

            }
        });
    }

    private void setListener() {

        imageViewSetting.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.imageViewSetting:

                Dialog.getInstance().logoutDialog(getContext());

                break;

            case R.id.imageViewDrawer:

                Boolean isChecked=SharedPreference.getInstance().getData(getContext());

                ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());


                if(fragmentCount==0)
                {
                    if(isChecked)
                    {
                            ReplaceFragment.getInstance().replace(((MainActivity) getActivity()), new FindGigsFragment());
                        } else {
                            ReplaceFragment.getInstance().replace((MainActivity) getActivity(), new FindCasteesFragment());
                        }

                    }

                    else if (fragmentCount==1)
                {
                    if (isChecked) {
                        ReplaceFragment.getInstance().replace((MainActivity) getActivity(), new ProfileFragment());
                    } else {
                        ReplaceFragment.getInstance().replace((MainActivity) getActivity(), new UserProfileFragment());
                    }
                }

                else if (fragmentCount==2)
                {
                    ReplaceFragment.getInstance().replace((MainActivity) getActivity(), new ActiveGigFragment());
                }

                else
                {
                    ReplaceFragment.getInstance().replace((MainActivity) getActivity(), new NotificationFragment());
                }

                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_back);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(10,10,10,10);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("Settings");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.GONE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.VISIBLE);

        ((MainActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }


}
