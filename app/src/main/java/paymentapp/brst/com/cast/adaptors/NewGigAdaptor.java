package paymentapp.brst.com.cast.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.ActiveGigModel;

/**
 * Created by brst-pc89 on 9/6/17.
 */

public class NewGigAdaptor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    int TYPE_HEADER = 0, TYPE_ITEM = 1;
    Context context;
    List<ActiveGigModel> listAddNewGig;
    public NewGigAdaptor(Context context, List<ActiveGigModel> listAddNewGig) {

        this.context=context;
        this.listAddNewGig=listAddNewGig;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView ;

        if (viewType == TYPE_ITEM)
        {
            itemView = LayoutInflater.from(context).inflate(R.layout.custom_itemaddnewgigs, parent, false);
            return new NewGigAdaptor.ItemViewHolder(itemView);

        }
        else
        {
            itemView = LayoutInflater.from(context).inflate(R.layout.custom_headergigs, parent, false);
            return new NewGigAdaptor.HeaderViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ActiveGigModel activeGigModel=listAddNewGig.get(position);

        if(holder instanceof NewGigAdaptor.ItemViewHolder)
        {
            ((NewGigAdaptor.ItemViewHolder) holder).textViewGigText.setText(activeGigModel.getGigName());
        }
        else if(holder instanceof NewGigAdaptor.HeaderViewHolder)
        {
            if(position==0)
            {
                ((HeaderViewHolder) holder).viewHeader.setVisibility(View.GONE);
            }

            ((NewGigAdaptor.HeaderViewHolder) holder).textViewGigHeader.setText(activeGigModel.getGigName());
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView textViewGigText;
        public ItemViewHolder(View itemView) {
            super(itemView);

            textViewGigText=(TextView)itemView.findViewById(R.id.textViewGigText);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView textViewGigHeader;
        View viewHeader;


        public HeaderViewHolder(View itemView) {
            super(itemView);

            textViewGigHeader=(TextView)itemView.findViewById(R.id.textViewGigHeader);
            viewHeader=(View) itemView.findViewById(R.id.viewHeader);

        }
    }

    @Override
    public int getItemCount() {
        return listAddNewGig.size();
    }

    @Override
    public int getItemViewType(int position) {

        if(isPositionHeader(position))
        {
            return TYPE_HEADER;
        }
        else
        {
            return TYPE_ITEM;
        }
    }

    private boolean isPositionHeader(int position) {

        return listAddNewGig.get(position).getType()==0;

    }


}
