package paymentapp.brst.com.cast.classes;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import paymentapp.brst.com.cast.frontScreens.LoginActivity;

/**
 * Created by brst-pc89 on 9/12/17.
 */

public class Dialog {

 private static Dialog instance=new Dialog();
    public static Dialog getInstance()
    {
        return instance;
    }

    public void logoutDialog(final Context context)
    {
      AlertDialog  alertDialog = new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage("Do you want to logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        Intent intent=new Intent(context, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);


                    }
                })
              .setNegativeButton("No",new DialogInterface.OnClickListener() {
                  public void onClick(DialogInterface dialog, int which) {
                      dialog.dismiss();


                  }
              })

                .show();
    }
}
