package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.classes.Dialog;
import paymentapp.brst.com.cast.mainScreens.MainActivity;
import paymentapp.brst.com.cast.sharedpreference.SharedPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileFragment extends Fragment implements View.OnClickListener{


    View view;

    public UserProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view==null) {
            view = inflater.inflate(R.layout.fragment_user_profile, container, false);



        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).imageViewDrawer.setImageResource(R.mipmap.ic_drawer);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setImageResource(R.mipmap.ic_logout);
        ((MainActivity) getActivity()).imageViewDrawer.setPadding(0,0,0,0);
        ((MainActivity) getActivity()).textViewDrawer.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).textViewDrawer.setText("Profile");
        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).imageViewDrawerSearch.setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).viewDrawer.setVisibility(View.GONE);


        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);

        ((MainActivity) getActivity()).imageViewDrawerSearch.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.imageViewDrawer:


                Boolean  isChecked= SharedPreference.getInstance().getData(getContext());
                if(!isChecked)
                {

                    if(!MainActivity.listNavigationText.contains("Invite Castees")) {
                        MainActivity.listNavigationText.add(1, "Invite Castees");
                    }
                }
                else
                {
                    if(MainActivity.listNavigationText.contains("Invite Castees")) {
                        MainActivity.listNavigationText.remove("Invite Castees");
                    }
                }

                MainActivity.navigationAdaptor.addData(MainActivity.listNavigationText);
                ((MainActivity) getActivity()).drawer.openDrawer(((MainActivity) getActivity()).navigationView);


                break;

            case R.id.imageViewDrawerSearch:

                Fragment fragment=getFragmentManager().findFragmentById(R.id.containerFL);
                if(fragment instanceof UserProfileFragment) {

                    Dialog.getInstance().logoutDialog(getContext());
                }
                break;
        }
    }

}
