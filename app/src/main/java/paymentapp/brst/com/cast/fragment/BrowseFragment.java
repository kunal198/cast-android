package paymentapp.brst.com.cast.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import paymentapp.brst.com.cast.R;
import paymentapp.brst.com.cast.adaptors.UserGridAdaptor;
import paymentapp.brst.com.cast.classes.ActiveGigModel;
import paymentapp.brst.com.cast.classes.ReplaceFragment;
import paymentapp.brst.com.cast.mainScreens.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class BrowseFragment extends Fragment implements View.OnClickListener{


    GridView gridviewUsers;

    List<ActiveGigModel> listUser;

    TextView textViewShortlist;

    View view;

    public BrowseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_users, container, false);

            bindViews();

            setAdaptor();
        }
        return view;
    }

    private void bindViews() {

        gridviewUsers = (GridView) view.findViewById(R.id.gridviewUsers);

        view.findViewById(R.id.imageViewUserBack).setOnClickListener(this);
        view.findViewById(R.id.textViewShortlist).setOnClickListener(this);

    }
    ActiveGigModel activeGigModel;
    private void setAdaptor()
    {
        listUser=new ArrayList<>();

        for(int i=0;i<=20;i++)
        {
            activeGigModel=new ActiveGigModel();
            activeGigModel.setGigName("username");
            if(i%2==0)
            {
                activeGigModel.setImage(R.mipmap.ic_user_rate);
            }
            else
            {
                activeGigModel.setImage(R.mipmap.ic_userrate);
            }

            listUser.add(activeGigModel);
        }

        UserGridAdaptor userGridAdaptor=new UserGridAdaptor(getContext(),listUser);
        gridviewUsers.setAdapter(userGridAdaptor);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).linearLayoutBottom.setVisibility(View.GONE);
        ((MainActivity) getActivity()).linearLayoutHeader.setVisibility(View.GONE);

        ((MainActivity) getActivity()).drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ((MainActivity) getActivity()).imageViewDrawer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.imageViewUserBack:

                ReplaceFragment.getInstance().popBackStack((MainActivity) getActivity());
                break;

            case R.id.textViewShortlist:


                ReplaceFragment.getInstance().replace((MainActivity) getActivity(),new ShortlistFragment());

                break;
        }
    }
}
